/*
  * Vector3.h
 *
 *  Created on: Jul 2, 2012
 *      Author: gpolles
 */

#ifndef POINT3D_H_
#define POINT3D_H_

#include <iostream>
#include <vector>
#include <cmath>
#include <stdexcept>


namespace mylib{

template <class T>
inline T SQ(T i){ return (i)*(i) ;}

template <class T>
class Vector3
{
//  friend std::ostream& operator <<(std::ostream &out, const Vector3<T>& p);
//  friend std::istream& operator >>(std::istream &in, Vector3<T>& p);


public:
  T X;
  T Y;
  T Z;
  public:
  Vector3(){X=0;Y=0;Z=0;}
  Vector3(T ax, T ay, T az) {X=ax;Y=ay;Z=az;}
  Vector3(const Vector3<T>& a) {X = a.X ; Y = a.Y ; Z = a.Z;}


  // Comparison operators
  bool operator ==(const Vector3<T>& a) { if(a.X == X && a.Y==Y && a.Z==Z) return true; return false;}
  bool operator !=(const Vector3<T>& a) { if(a.X == X && a.Y==Y && a.Z==Z) return false; return true;}

  // vector-vector operators
  inline Vector3<T> operator-(const Vector3<T>& a) const{
      return Vector3<T>(X-a.X,Y-a.Y,Z-a.Z);
  }
  inline Vector3<T> operator-() const{
        return Vector3<T>(-X,-Y,-Z);
  }
  inline Vector3<T> operator+(const Vector3<T>& a) const{
      return Vector3<T>(X+a.X,Y+a.Y,Z+a.Z);
  }
  inline Vector3<T> operator*(const Vector3<T>& a) const{
        return Vector3<T>(X*a.X,Y*a.Y,Z*a.Z);
  }
  inline Vector3<T> operator/(const Vector3<T>& a) const{
        return Vector3<T>(X/a.X,Y/a.Y,Z/a.Z);
  }

  // Self-vector operators
  inline Vector3<T>& operator-=(const Vector3<T>& a) {
      X-=a.X; Y-=a.Y; Z-=a.Z;
      return *this;
  }
  inline Vector3<T>& operator+=(const Vector3<T>& a) {
        X+=a.X; Y+=a.Y; Z+=a.Z;
        return *this;
  }
  inline Vector3<T>& operator/=(const Vector3<T>& a) {
      X/=a.X; Y/=a.Y; Z/=a.Z;
      return *this;
  }
  inline Vector3<T>& operator*=(const Vector3<T>& a) {
        X*=a.X; Y*=a.Y; Z*=a.Z;
        return *this;
  }


  // Self-scalar operators
  inline Vector3<T>& operator/=(const T a) {
      X/=a; Y/=a; Z/=a;
      return *this;
  }
  inline Vector3<T>& operator*=(const T a) {
    X*=a; Y*=a; Z*=a;
    return *this;
  }
  inline Vector3<T>& operator-=(const T a) {
      X-=a; Y-=a; Z-=a;
      return *this;
  }
  inline Vector3<T>& operator+=(const T a) {
    X+=a; Y+=a; Z+=a;
    return *this;
  }

  // Vector-scalar operators
  inline Vector3<T> operator*(T a) const{
        return Vector3<T>(X*a,Y*a,Z*a);
  }
  inline Vector3<T> operator/(T a) const{
        return Vector3<T>(X/a,Y/a,Z/a);
  }
  inline Vector3<T> operator+(T a) const{
        return Vector3<T>(X+a,Y+a,Z+a);
  }
  inline Vector3<T> operator-(T a) const{
        return Vector3<T>(X-a,Y-a,Z-a);
  }

  T* asPointer(){
    return &X;
  }


  T& operator[](size_t i) {
    if (i==0)
      return X;
    if (i==1)
      return Y;
    else
      return Z;
    //return *(&X+i);
  }
  const T& operator[](size_t i) const{
    if (i==0)
      return X;
    if (i==1)
      return Y;
    else
      return Z;
    //return *(&X+i);
  }

  inline T dotProduct(const Vector3<T>& p) const {return X*p.X + Y*p.Y + Z*p.Z;}
  inline Vector3<T> crossProduct(const Vector3<T>& p) const {
    return Vector3<T>( Y*p.Z - Z*p.Y,
                    Z*p.X - X*p.Z,
                    X*p.Y - Y*p.X);
  }


  inline T distance(const Vector3<T>& p) const {return sqrt(SQ(p.X-X)+SQ(p.Y-Y)+SQ(p.Z-Z)); }
  inline T distanceSQ(const Vector3<T>& p) const {return SQ(p.X-X)+SQ(p.Y-Y)+SQ(p.Z-Z); }

  inline T normSQ() const {return X*X + Y*Y + Z*Z;}
  inline T norm() const {return sqrt(X*X + Y*Y + Z*Z);}

  inline Vector3<T>& normalize(){
    if (X==0 && Y==0 && Z==0)
    {
      throw std::domain_error("Cannot normalize null vector");
    }
    T n = 1.0/sqrt(X*X + Y*Y + Z*Z);
    X *= n;Y *= n; Z *= n;
    return *this;
  }

  Vector3<T> normalized() const{
    if (X==0 && Y==0 && Z==0)
    {
      throw std::domain_error("Cannot normalize null vector");
    }
    T n = 1.0/sqrt(X*X + Y*Y + Z*Z);
    return Vector3<T>(X*n, Y*n, Z*n);
  }

  inline T angle(const Vector3<T>& p) const {
    T x = dotProduct(p)/norm()/p.norm();
    return acos( x );
  }
  inline T cosAngle(const Vector3<T>& p) const {
    return dotProduct(p)/norm()/p.norm();
  }

  Vector3<T>& rotate(const Vector3<T>& axis, const T angle){
    Vector3<T> normaxis=axis/axis.norm();
    Vector3<T> par = normaxis*(dotProduct(normaxis));
    Vector3<T> k = normaxis.crossProduct(*this);
    *this = par + (*this-par)*T(cos(angle)) + k*T(sin(angle));
    return *this;
  }

  Vector3<T>& projectOn(const Vector3<T>& u){
    *this = u*(dotProduct(u)/u.normSQ());
    return *this;

  }

  Vector3<T>& projectOrthogonally(const Vector3<T>& u){ // project on orthogonal plane
    *this -= u*(dotProduct(u)/u.normSQ());
    return *this;
  }
};


class box_t: public std::vector<Vector3<double> >{
  public:
    box_t() {resize(2);}
    Vector3<double> lengths() const{
      return at(1)-at(0);
    }
};

template <class T>
inline T norm(const Vector3<T>& v) {return sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z);}
template <class T>
inline T normSQ(const Vector3<T>& v) {return (v.X*v.X + v.Y*v.Y + v.Z*v.Z);}
template <class T>
inline T distance(const Vector3<T>& p,const Vector3<T>& q)  {return sqrt(SQ(p.X-q.X)+SQ(p.Y-q.Y)+SQ(p.Z-q.Z)); }
template <class T>
inline T distanceSQ(const Vector3<T>& p,const Vector3<T>& q)  {return SQ(p.X-q.X)+SQ(p.Y-q.Y)+SQ(p.Z-q.Z); }

inline std::ostream& operator <<(std::ostream &out, const Vector3<float>& p){
      out << " " << p.X << " " <<  p.Y << " " << p.Z ;
      return out;
}

inline std::ostream& operator <<(std::ostream &out, const Vector3<double>& p){
      out << " " << p.X << " " <<  p.Y << " " << p.Z ;
      return out;
}


template <class T>
inline std::istream& operator >>(std::istream &in, Vector3<T>& p){
        in >> p.X >>  p.Y >> p.Z;
        return in;
}

template <class T>
inline Vector3<T> operator *(const T x, const Vector3<T>& v) {
  return v*x;
}

template <class T>
inline T dotProduct(const Vector3<T>& p,const Vector3<T>& q) {
  return q.X*p.X + q.Y*p.Y + q.Z*p.Z;
}

template <class T>
inline Vector3<T> crossProduct(const Vector3<T>& v,const Vector3<T>& p) {
  return v.crossProduct(p);
}

template <class T>
inline Vector3<T> rotate(const Vector3<T>& v, const Vector3<T>& axis, const T angle){
  Vector3<T> normaxis=axis/norm(axis);
  Vector3<T> par = dotProduct(v,normaxis)*normaxis;
  Vector3<T> k = crossProduct(normaxis,v);
  return par + T(cos(angle))*(v-par) + T(sin(angle))*k ;
}

template <class T>
inline Vector3<T> normalize(const Vector3<T>& v){
  return v/v.norm();
}

template <class T>
inline Vector3<T> distanceVectorPbc(const Vector3<T>& p,const Vector3<T>& q,const Vector3<T>& box_l){
  Vector3<T> d = (q-p)/box_l;
  d.X = ( d.X - round(d.X) )*box_l.X;
  d.Y = ( d.Y - round(d.Y) )*box_l.Y;
  d.Z = ( d.Z - round(d.Z) )*box_l.Z;
  return d;
}

template <class T>
inline Vector3<T> distanceVectorPbc(const Vector3<T>& p,
                                    const Vector3<T>& q,
                                    const Vector3<T>& box_l,
                                    bool xflag, bool yflag, bool zflag){
  Vector3<T> d = (q-p)/box_l;
  if(xflag){
    d.X = ( d.X - round(d.X) )*box_l.X;
  }
  else{
    d.X *= box_l.X;
  }

  if(yflag){
    d.Y = ( d.Y - round(d.Y) )*box_l.Y;
  }
  else{
    d.Y *= box_l.Y;
  }

  if(zflag){
    d.Z = ( d.Z - round(d.Z) )*box_l.Z;
  }
  else{
    d.Z *= box_l.Z;
  }

  return d;
}


template <class T>
inline Vector3<T> closest_image(const Vector3<T>& p,const Vector3<T>& ref, const Vector3<T>& box_l){
  return ref + distanceVectorPbc(ref, p, box_l);
}

template <class T>
inline Vector3<T> closest_image(const Vector3<T>& p,const Vector3<T>& ref, const Vector3<T>& box_l,
    bool flag_x, bool flag_y, bool flag_z){
  return ref + distanceVectorPbc(ref, p, box_l, flag_x, flag_y, flag_z);
}

template <class T>
inline T distanceSqPbc(const Vector3<T>& p,const Vector3<T>& q,const Vector3<T>& box_l){
  return distanceVectorPbc(p,q,box_l).normSQ();
}

template <class T>
inline T distanceSqPbc(const Vector3<T>& p, const Vector3<T>& q, const Vector3<T>& box_l,
                       bool xflag, bool yflag, bool zflag){
  return distanceVectorPbc(p, q, box_l, xflag, yflag, zflag).normSQ();
}


template <class T>
inline T distancePbc(const Vector3<T>& p, const Vector3<T>& q, const Vector3<T>& box_l,
                     bool xflag, bool yflag, bool zflag){
  return sqrt(distanceVectorPbc(p, q, box_l, xflag, yflag, zflag).normSQ());
}

template <class T>
inline T distancePbc( const Vector3<T>& p,
                      const Vector3<T>& q,
                      const Vector3<T>& box_l){
  return sqrt(distanceVectorPbc(p, q, box_l).normSQ());
}

inline Vector3<double> PbcWrap(const Vector3<double>& p, const box_t& box){
  Vector3<double> box_l = box.lengths();
  Vector3<double> d = p/box_l;
  d.X = ( d.X - round(d.X) )*box_l.X;
  d.Y = ( d.Y - round(d.Y) )*box_l.Y;
  d.Z = ( d.Z - round(d.Z) )*box_l.Z;
  return box[0]+d;
}

template <class T>
inline T cosAngle(const Vector3<T>& p,const Vector3<T>& q) {
    return dotProduct(p,q)/norm(p)/norm(q);
}

template <class T>
inline T angle(const Vector3<T>& p,const Vector3<T>& q) {
    return acos(dotProduct(p,q)/norm(p)/norm(q));
}

template <class T>
inline Vector3<T> x_rotation(const Vector3<T>& v, T angle){
  T sint=sin(angle);
  T cost=cos(angle);
  return Vector3<T>(v.X,v.Y*cost+v.Z*sint,v.Y*(-sint)+v.Z*cost);
}

template <class T>
inline Vector3<T> y_rotation(const Vector3<T>& v, T angle){
  T sint=sin(angle);
  T cost=cos(angle);
  return Vector3<T>(v.X*cost+v.Z*(-sint),v.Y,v.X*sint+v.Z*cost);
}

template <class T>
inline Vector3<T> z_rotation(const Vector3<T>& v, T angle){
  T sint=sin(angle);
  T cost=cos(angle);
  return Vector3<T>(v.X*cost+v.Y*sint,v.X*(-sint)+v.Y*cost,v.Z);
}

template <class T>
inline Vector3<T> parallelProjection(const Vector3<T>& v, const Vector3<T>& u){
  return u*(dotProduct(v,u)/normSQ(u));
}

template <class T>
inline Vector3<T> orthogonalProjection(const Vector3<T>& v, const Vector3<T>& u){
  return v-u*(dotProduct(v,u)/normSQ(u));
}

namespace{
const Vector3<double> x_axis = Vector3<double>(1,0,0);
const Vector3<double> y_axis = Vector3<double>(0,1,0);
const Vector3<double> z_axis = Vector3<double>(0,0,1);
}

typedef Vector3<double> Vector3d;
typedef Vector3<double> Vector3f;

} // namespace mylib
#endif /* POINT3D_H_ */
